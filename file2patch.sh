#!/usr/bin/env bash
# convert a text file to a patch file
set -Eeuo pipefail

# usage: file2patch "source/file" "relative/destination/path" "patch/file"
file2patch() {
    local SRCFILE="$1"
    local PATCHPATH="$2"
    local OUTFILE="$3"
    local FILEMODE="$(stat -c '%a' "${SRCFILE}")"
    local LINECOUNT="$(wc -l "${SRCFILE}" | cut -d' ' -f1)"

    (
        printf "diff --git a/%s b/%s\n" "${PATCHPATH}" "${PATCHPATH}"
        printf "new file mode 100%s\n" "${FILEMODE}"
        printf "index 00000000..11451419\n"
        printf "%s\n" "--- /dev/null" # "printf --" will cause parsing problems
        printf "+++ b/%s\n" "${PATCHPATH}"
        printf "@@ -0,0 +1,%d @@\n" "${LINECOUNT}"
        cat "${SRCFILE}" | sed -e 's/^/+/g'
    ) > "${OUTFILE}"

}

file2patch "$@"
